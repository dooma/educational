Lectures = new Meteor.Collection('lectures');

Accounts.validateNewUser(function (user){
  if(user.emails[0].address && user.emails[0].address.length > 4)
    return true;
  throw new Meteor.Error(403, 'Email must have at least 5 characters');
});
Accounts.validateNewUser(function (user){
  if(user.profile.name)
    return true;
  throw new Meteor.Error(403, 'Name can not be blank');
});

Accounts.validateNewUser(function (user){
  console.log(user.profile.clas);
  if(user.profile.clas && parseInt(user.profile.clas))
    return true;
  throw new Meteor.Error(403, 'Class should be under 12');
});

Meteor.methods({
  insertLecture: function (title, content){
    Lectures.insert({title: title, content: content});
  },

  insertQuestion: function (data, lecture_id){
    Lectures.update({_id: lecture_id}, {$set: {question: data}});
  },
  deleteLecture: function (lecture_id){
    Lectures.remove({_id: lecture_id});
  }
});

Meteor.startup(function () {
});
