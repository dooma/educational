Lectures = new Meteor.Collection('lectures');

Template.accounts.signupTemplate = function(){
  return Session.get('signup') || false;
};
Template.main.admin = function (){
  if(!Meteor.user()) return false;
  return Meteor.user().profile.admin;
};
Template.main.readLecture = function (){
  if(typeof Session.get('lecture_id') == 'undefined' || Session.get('lecture_id') == null)
    return false;
  return true;
}
Template.main.showUsers = function (){
  if(typeof Session.get('showUsers') == 'undefined' || Session.get('showUsers') == false)
    return false;
  return true;
}

Template.signin.events({
  'click #signInForm input[type="submit"]': function (event, t){
    event.preventDefault();

    var email = t.find('#signInForm #email').value,
        password = t.find('#signInForm #password').value;

    Meteor.loginWithPassword(email, password, function(err){
      if(err)
        console.log(err);
      else
        Session.set('admin', Meteor.user().profile.admin);
    });


  },
  'click .signup': function(event, t){
    Session.set('signup', true);
  }
});

Template.signup.events({
  'click #signUpForm input[type="submit"]': function (event, t){
    event.preventDefault();

    var email = t.find('#signUpForm #email').value,
        password = t.find('#signUpForm #password').value,
        name = t.find('#signUpForm #name').value,
        clas = t.find('#signUpForm #class').value;

    console.log(email.length);
    Accounts.createUser({
      email: email,
      password: password,
      profile: {
        name: name,
        clas: clas.replace(/\D{1,2}/, '')
      }
    }, function(err) {
      if(err)
        console.log(err);
    });
  }
});

Template.signout.events({
  'click input': function(event, t){
    event.preventDefault();

    Meteor.logout(function(err){
      if(err)
        console.log(err);
    });
  }
});

Template.newLecture.rendered = function (t){
  $('#summernote').summernote();
};

Template.newLecture.events({
  'click #save': function(event, t){
    var content = $('#summernote').code(), title = t.find('#title').value;
    if(title.length == 0){
      throw new Meteor.Error(403, 'Title can not be empty');
    }

    Meteor.call('insertLecture', title, content, function (err, result){
      if(err)
        console.log(err);
    });
  }
});

Template.listLectures.lectures = function (){
  return Lectures.find();
};
Template.listLectures.admin = function (){
  if(!Meteor.user()) return false;
  return Meteor.user().profile.admin;
};
Template.listLectures.events({
  'click i': function (event, t){
    Meteor.call('deleteLecture', this._id);
  }
});

Template.title.events({
  'click': function (){
    Session.set('lecture_id', this._id);
  }
});

Template.lecture.lecture = function (){
  return Lectures.find({_id: Session.get('lecture_id')});
};

Template.lecture.events({
  'click i': function (){
    Session.set('lecture_id', null);
  }
});

Template.quizBuilder.rendered = function (){
  localStorage.quiz = null
  $('[data-quiz="auto"]').each(function () {
    var $quiz = $(this);
    $quiz.quizBuilder($quiz.data());
  })
};

Template.quizBuilder.events({
  'click button': function(event, t){
    if(localStorage.quiz == null)
      console.log('Va rog introduceti o intrebare');
    Meteor.call('insertQuestion', localStorage.quiz, Session.get('lecture_id'), function (err, result){
      if(err)
        console.log(err);
    });
  }
});

Template.quizSolver.question = function (){
  var lecture = Lectures.findOne({_id: Session.get('lecture_id')}, {question: 1});
  if(!lecture.question) return []

  var question = JSON.parse(lecture.question);
  for(var i in question){
    question[i][question[i].type] = true;
  }
  return question;
};

Template.quizSolver.correct = function (){
    if(Session.get('answer') === 'correct' || Session.get('answer') == 'wrong')
        return true;
    return false;
};

Template.quizSolver.events({
  'click button': function (){
    var lecture = Lectures.findOne({_id: Session.get('lecture_id')}, {question: 1});
    if(!lecture.question) return;
    var question = JSON.parse(lecture.question);

    for(var i in question){
      if(question[i].type == 'text'){
        if($('#'+question[i].content+' textarea').val() == question[i].options[0].content){
            Session.set('answer', 'correct');
        }
        else{
            Session.set('answer', 'wrong');
        }
      }
    }

  }
});

Template.correctAnswer.rendered = function (){
    Session.set('answer', null);
};

Template.correctAnswer.correct = function (){
    if(Session.get('answer') === 'correct')
        return true;
    return false;
}

Template.main.events({
  'click #users': function (){
    if(Session.get('showUsers') == true){
      Session.set('showUsers', false);
      $('#users').html('Arata utilizatori');
    } else {
      Session.set('showUsers', true);
      $('#users').html('Ascunde utilizatori');
      $('#summernote').destroy();
    }
  }
});

Template.users.users = function (){
  return Meteor.users.find();
};
